# Frontend - Proyecto de cafetería
Proyecto solicitado en el bootcamp

# Que necesitamos?

* React JS
* Vite
* Axios
* Bootstrap
* react-dom

# Levantar el proyecto
npm run dev

# Arquitectura general del proyecto cafeteria
![Diagrama de Arquitectura](arquitectura_general.jpeg)

# Esquema de componentes
![Componentes](Nav-cafeteria.jpeg)