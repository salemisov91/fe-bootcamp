import { useState, useEffect } from 'react'
import PedidoModal from './PedidoModal';
import 'bootstrap/dist/css/bootstrap.min.css';
import food from '../assets/food.png'
import Home from '../Home'

const Productos = () => {
    const [productosList, setProductos] = useState([])

    useEffect(() => {
        fetch('http://localhost:8000/productos/', {
            method: 'GET' /* or POST/PUT/PATCH/DELETE */,
            headers: {
                Authorization: `Bearer ${JSON.parse(window.localStorage.getItem('accessToken'))}`,
                'Content-Type': 'application/json',
            },
        })
            .then((res) => res.json())
            .then((data) => {
                setProductos(data)
            })
    }, [])

    const handleChangeName = (event) => {
        setName(event.target.value)
    }

    const [productosSeleccionados, setProductosSeleccionados] = useState([]);

    const agregarProducto = (producto) => {
        setProductosSeleccionados([...productosSeleccionados, producto]);
    };

    const borrarTodosLosProductos = () => {
        setProductosSeleccionados([]);
    };

    const quitarProducto = (producto) => {
        const nuevosProductos = productosSeleccionados.filter(
        (p) => p.id !== producto.id
        );
        setProductosSeleccionados(nuevosProductos);
    };

    const [modalOpen, setModalOpen] = useState(false);

    const handleOpenModal = () => {
        setModalOpen(true);
    };

    const handleCloseModal = () => {
        setModalOpen(false);
    };

    return (
        <>
            <h2>Productos para preparar la orden</h2>
            <ul>
                {productosList.map((producto) => {
                    return <li key={producto.id}>
                                <div>
                                    <h2>
                                    {producto.nombre}
                                    </h2>
                                    <img src={food} />
                                    <p>Precio: {producto.precio}</p>
                                    <button onClick={() => agregarProducto(producto)}>Agregar</button>
                                </div>
                            </li>
                })}
            </ul>
            <hr></hr>
            <button onClick={handleOpenModal} variant="primary">Confirmar el pedido</button>
            <button onClick={() => borrarTodosLosProductos()} variant="primary">Cargar nuevo pedido</button>
            <PedidoModal
                isOpen={modalOpen}
                onClose={handleCloseModal}
                productosSeleccionados={productosSeleccionados}
            />
            <h2>Productos seleccionados</h2>
            <ul>
                {productosSeleccionados.map((producto) => {
                    return <li key={producto.id}>
                                <div>
                                    <h2>
                                    {producto.nombre}
                                    </h2>
                                    <img src={food} />
                                    <p>Precio: {producto.precio}</p>
                                    <button onClick={() => quitarProducto(producto)}>Quitar</button>
                                </div>
                            </li>
                })}
            </ul>
        </>
    )
}

export default Productos;