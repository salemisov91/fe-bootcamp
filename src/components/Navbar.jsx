import React, { useState } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import ReactDOM from 'react-dom';
import Home from '../Home'

const Navbar = ({ logoutHandler, userId }) => {

  const handlePedidosClick = () => {
    ReactDOM.render(
      <Home onLogout={logoutHandler} userId={userId} item="Pedidos" />,
      document.getElementById('root')
    );
  };

  const handleHomeClick = () => {
    ReactDOM.render(
      <Home onLogout={logoutHandler} userId={userId} item="Producto" />,
      document.getElementById('root')
    );
  };

  return (
    <nav>
      <ul>
        <li><a href="#" onClick={handleHomeClick}>Inicio</a></li>
        <li><a href="#" onClick={handlePedidosClick}>Pedidos</a></li>
        <li><a href="#" onClick={logoutHandler}>Cerrar Sesión</a></li>
      </ul>
    </nav>
  );
}

export default Navbar;