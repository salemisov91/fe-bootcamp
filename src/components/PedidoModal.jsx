import { Modal, Form } from "react-bootstrap";
import React from 'react';
import { Button } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import { useState, useEffect } from 'react'
import axios from "axios";

const PedidoModal = ({ isOpen, onClose, productosSeleccionados }) => {
  const total = productosSeleccionados.reduce(
    (acc, producto) => acc + producto.precio,
    0
  );

  const [nombreCliente, setNombreCliente] = useState('');

  useEffect(() => {
    if (isOpen) {
      setNombreCliente('');
    }
  }, [isOpen]);

  const handleSubmit = (event) => {
    const pedido = {
      mesa: nombreCliente,
      estado: 'PREPARANDO',
      lista_productos: productosSeleccionados.map(producto => ({
        id: producto.id,
        nombre: producto.nombre,
        precio: producto.precio
      }))
    };
    event.preventDefault();
  
    axios.post('http://localhost:8000/pedidos/', pedido, {
      headers: {
        Authorization: `Bearer ${JSON.parse(window.localStorage.getItem('accessToken'))}`,
        'Content-Type': 'application/json',
      }
    })
    .then(response => {
      console.log(response);
      onClose(); // Cerrar el modal si la solicitud fue exitosa
    })
    .catch(error => {
      console.log(error);
    });
  }

  return (
    <Modal show={isOpen} onHide={onClose}>
      <Modal.Header closeButton>
        <Modal.Title>Confirmar Pedido</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form.Group>
            <Form.Label>Nombre del cliente:</Form.Label>
            <Form.Control
              type="text"
              placeholder="Ingrese el nombre del cliente"
              value={nombreCliente}
              onChange={(e) => setNombreCliente(e.target.value)}
            />
        </Form.Group>
        <p>Total del pedido (Gs.): {total}</p>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={onClose}>
          Cancelar
        </Button>
        <Button variant="primary" onClick={handleSubmit}>
          Confirmar Pedido
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default PedidoModal;
