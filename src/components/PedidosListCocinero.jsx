import React, { useEffect, useState } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import axios from "axios";

const PedidosListCocinero = () => {
  const [pedidos, setPedidos] = useState([]);

    useEffect(() => {
        axios.get('http://localhost:8000/pedidos/?estado=PREPARANDO', {
            headers: {
              Authorization: `Bearer ${JSON.parse(window.localStorage.getItem('accessToken'))}`,
              'Content-Type': 'application/json',
            }
          })
          .then(response => {
            setPedidos(response.data);
          })
          .catch(error => {
            console.log(error);
          });
    }, [])
    
    const handleCompletar = (pedido_param) => {
        const pedido = {
          mesa: pedido_param.mesa,
          estado: 'COMPLETADO',
          lista_productos: pedido_param.lista_productos
        };
    
        axios.put(`http://localhost:8000/pedidos/${pedido_param.id}/`, pedido, {
          headers: {
            Authorization: `Bearer ${JSON.parse(window.localStorage.getItem('accessToken'))}`,
            'Content-Type': 'application/json',
          }
        })
        .then(response => {
            const pedidoCompletado = pedidos.filter(p => p.id !== pedido_param.id);
            setPedidos(pedidoCompletado);
          })
        .catch(error => {
          console.log(error);
        });
    }

  return (
    <>
      <h1>Listado de pedidos</h1>
      <table>
        <thead>
          <tr>
            <th>Mesa</th>
            <th>Estado</th>
            <th>Productos</th>
            <th>Acciones</th>
          </tr>
        </thead>
        <tbody>
          {pedidos.map(pedido => (
            <tr key={pedido.id}>
              <td>{pedido.mesa}</td>
              <td>{pedido.estado}</td>
              <td>
                <ul>
                  {pedido.lista_productos.map(producto => (
                    <li key={producto.id}>
                      {producto.nombre} - {producto.precio}
                    </li>
                  ))}
                </ul>
              </td>
              <td>
                <button onClick={() => handleCompletar(pedido)}>
                  Completar
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </>
  );
};

export default PedidosListCocinero;