import React, { useEffect, useState } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import axios from "axios";

const PedidosList = () => {
  const [pedidos, setPedidos] = useState([]);

    useEffect(() => {
        axios.get('http://localhost:8000/pedidos/?estado=COMPLETADO', {
            headers: {
              Authorization: `Bearer ${JSON.parse(window.localStorage.getItem('accessToken'))}`,
              'Content-Type': 'application/json',
            }
          })
          .then(response => {
            setPedidos(response.data);
          })
          .catch(error => {
            console.log(error);
          });
    }, [])
    
    const handleEntregar = (pedido_param) => {
        const pedido = {
          mesa: pedido_param.mesa,
          estado: 'ENTREGADO',
          lista_productos: pedido_param.lista_productos
        };
    
        axios.put(`http://localhost:8000/pedidos/${pedido_param.id}/`, pedido, {
          headers: {
            Authorization: `Bearer ${JSON.parse(window.localStorage.getItem('accessToken'))}`,
            'Content-Type': 'application/json',
          }
        })
        .then(response => {
            const pedidoEntregado = pedidos.filter(p => p.id !== pedido_param.id);
            setPedidos(pedidoEntregado);
          })
        .catch(error => {
          console.log(error);
        });
    }

  return (
    <>
      <h1>Listado de pedidos</h1>
      <table>
        <thead>
          <tr>
            <th>Mesa</th>
            <th>Estado</th>
            <th>Productos</th>
            <th>Acciones</th>
          </tr>
        </thead>
        <tbody>
          {pedidos.map(pedido => (
            <tr key={pedido.id}>
              <td>{pedido.mesa}</td>
              <td>{pedido.estado}</td>
              <td>
                <ul>
                  {pedido.lista_productos.map(producto => (
                    <li key={producto.id}>
                      {producto.nombre} - {producto.precio}
                    </li>
                  ))}
                </ul>
              </td>
              <td>
                <button onClick={() => handleEntregar(pedido)}>
                  Entregar
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </>
  );
};

export default PedidosList;