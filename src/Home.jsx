import { useState, useEffect } from 'react'
import Productos from './components/Productos'
import Navbar from "./components/Navbar";
import 'bootstrap/dist/css/bootstrap.min.css';
import PedidosList from "./components/PedidosList";
import PedidosListCocinero from "./components/PedidosListCocinero";

const Home = ({ onLogout, userId, item }) => {
  const [user, setUser] = useState()

  useEffect(() => {
    fetch('http://localhost:8000/users/' + userId, {
      method: 'GET' /* or POST/PUT/PATCH/DELETE */,
      headers: {
        Authorization: `Bearer ${JSON.parse(window.localStorage.getItem('accessToken'))}`,
        'Content-Type': 'application/json',
      },
    })
      .then((res) => res.json())
      .then((userData) => {
        setUser(userData)
      })
  }, [])

  

  const logoutHandler = () => {
    onLogout()
  }

  const role = user ? user.group_name : null

  const content =
  role && role === 'recepcionista'
    ? item === 'Producto'
      ? <Productos />
      : item === 'Pedidos'
        ? <PedidosList />
        : <p>Home Page</p>
        : role === 'cocinero'
        ? <PedidosListCocinero />
    : <p>Home Page</p>;

  return (
    <>
      <Navbar logoutHandler={onLogout} userId={userId}/>
      {user && <>
        <h1>Bienvenido {user.username}!</h1>
        <hr></hr>
        {content}
      </>}
    </>
  )
}

export default Home;